package exceptions;

public class NoConnectionException extends Exception {

    public NoConnectionException() {
        super();
    }

    public NoConnectionException(String message) {
        super(message);
    }

    public NoConnectionException(Exception exception) {
        super(exception);
    }

    public NoConnectionException(String message, Exception exception) {
        super(message, exception);
    }

}
