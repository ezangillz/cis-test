package exceptions;

public class CouldNotSaveXml extends Exception {

     public CouldNotSaveXml() {
        super();
    }

    public CouldNotSaveXml(String message) {
        super(message);
    }

    public CouldNotSaveXml(Exception exception) {
        super(exception);
    }

    public CouldNotSaveXml(String message, Exception exception) {
        super(message, exception);
    }

}
