package exceptions;

public class CouldNotFetchRecords extends Exception {

    public CouldNotFetchRecords() {
        super();
    }

    public CouldNotFetchRecords(String message) {
        super(message);
    }

    public CouldNotFetchRecords(Exception exception) {
        super(exception);
    }

    public CouldNotFetchRecords(String message, Exception exception) {
        super(message, exception);
    }

}
