package exceptions;

public class CouldNotSaveRecords extends Exception {

    public CouldNotSaveRecords() {
        super();
    }

    public CouldNotSaveRecords(String message) {
        super(message);
    }

    public CouldNotSaveRecords(Exception exception) {
        super(exception);
    }

    public CouldNotSaveRecords(String message, Exception exception) {
        super(message, exception);
    }

}
