package exceptions;

public class CouldNotCreateXml extends Exception {

     public CouldNotCreateXml() {
        super();
    }

    public CouldNotCreateXml(String message) {
        super(message);
    }

    public CouldNotCreateXml(Exception exception) {
        super(exception);
    }

    public CouldNotCreateXml(String message, Exception exception) {
        super(message, exception);
    }

}
