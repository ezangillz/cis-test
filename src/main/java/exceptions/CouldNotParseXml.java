package exceptions;

public class CouldNotParseXml extends Exception {

     public CouldNotParseXml() {
        super();
    }

    public CouldNotParseXml(String message) {
        super(message);
    }

    public CouldNotParseXml(Exception exception) {
        super(exception);
    }

    public CouldNotParseXml(String message, Exception exception) {
        super(message, exception);
    }

}
