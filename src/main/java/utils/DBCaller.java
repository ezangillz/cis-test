package utils;

import exceptions.CouldNotFetchRecords;
import exceptions.CouldNotSaveRecords;
import exceptions.NoConnectionException;
import model.Record;
import model.RecordKey;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * A class to make calls to the database
 */
public class DBCaller {


    public static final String DEP_CODE_KEY = "dep_code";
    public static final String DEP_JOB_KEY = "dep_job";
    public static final String DESCRIPTION_STRING_KEY = "description_string";

    private static final Logger log = LogManager.getLogger(DBCaller.class);

    private static final String GET_ALL_RECORDS_QUERY = "SELECT * FROM records";
    private static final String INSERT_RECORDS_QUERY = "INSERT INTO records(" + DEP_CODE_KEY + ", " + DEP_JOB_KEY + ", " + DESCRIPTION_STRING_KEY + ") VALUES(?, ?, ?)";
    private static final String DELETE_RECORDS_QUERY = "DELETE FROM records WHERE " + DEP_CODE_KEY + " = ? AND " + DEP_JOB_KEY + " = ?";

    /**
     * Synchronizes the database with the records param
     * @param records parameter that is taken from the XML file with all the records
     * @throws CouldNotSaveRecords if could not establish connection with the database
     */
    public void setAllRecords(HashMap<RecordKey, Record> records)
                              throws CouldNotSaveRecords {
        try {
            log.info("Syncing the database with the XML");
            log.info("Getting the records from the database for comparison");
            HashMap<RecordKey, Record> oldRecords = getAllRecords();
            Set<RecordKey> toDelete = ((HashMap<RecordKey, Record>) oldRecords.clone()).keySet();
            toDelete.removeAll(records.keySet());

            log.info("Preparing statements for inserts and deletes");
            Connection connection = ConnectionFetcher.getConnection();
            connection.setAutoCommit(false);
            PreparedStatement insertStatement = connection
                    .prepareStatement(INSERT_RECORDS_QUERY);
            PreparedStatement deleteStatement = connection
                    .prepareStatement(DELETE_RECORDS_QUERY);
            log.info("Updating and inserting records");
            for (Map.Entry<RecordKey, Record> entry : records.entrySet()) {
                if (!oldRecords.containsKey(entry.getKey()) ||
                        (oldRecords.containsKey(entry.getKey()) && !oldRecords.get(entry.getKey()).equals(entry.getValue()))) {
                    insertStatement.setString(1, entry.getKey().getDepCode());
                    insertStatement.setString(2, entry.getKey().getDepJob());
                    insertStatement.setString(3, entry.getValue().getDescriptionString());
                    log.debug("Key to [INSERT]: job: " + entry.getKey().getDepJob() + ", code: " + entry.getKey().getDepCode());
                    insertStatement.addBatch();
                }
            }

            log.info("Deleting records that are absent in the XML");
            for (Iterator<RecordKey> it = toDelete.iterator(); it.hasNext();) {
                RecordKey key = it.next();
                deleteStatement.setString(1, key.getDepCode());
                deleteStatement.setString(2, key.getDepJob());
                log.debug("Key to [DELETE]: job: " + key.getDepJob() + ", code: " + key.getDepCode());
                deleteStatement.addBatch();
            }

            insertStatement.executeBatch();
            deleteStatement.executeBatch();

            connection.commit();

            insertStatement.close();
            deleteStatement.close();
            connection.close();
        } catch (NoConnectionException | SQLException | CouldNotFetchRecords e) {
            throw new CouldNotSaveRecords("Could not save records to the database", e);
        }

    }

    /**
     * Gets all records from the database
     * @return all the records from the database
     * @throws CouldNotFetchRecords if records could not be fetched from the database for some reason
     */
    public HashMap<RecordKey, Record> getAllRecords() throws CouldNotFetchRecords {
        Connection connection = null;
        Statement statement = null;
        HashMap<RecordKey, Record> records = new HashMap<>();

        try {
            log.info("Getting all records from the database");
            connection = ConnectionFetcher.getConnection();
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(GET_ALL_RECORDS_QUERY);
            while (resultSet.next()) {
                Record record = new Record(resultSet.getString(DESCRIPTION_STRING_KEY));
                RecordKey key = new RecordKey(resultSet.getString(DEP_CODE_KEY),
                        resultSet.getString(DEP_JOB_KEY));
                records.put(key, record);
            }

            statement.close();
            connection.close();
        } catch (NoConnectionException | SQLException e) {
            throw new CouldNotFetchRecords("Records could not be fetched", e);
        }

        return records;
    }

}
