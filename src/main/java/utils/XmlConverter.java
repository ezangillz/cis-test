package utils;

import exceptions.CouldNotCreateXml;
import exceptions.CouldNotParseXml;
import exceptions.CouldNotSaveRecords;
import exceptions.CouldNotSaveXml;
import model.Record;
import model.RecordKey;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * A class to work with XMLs
 */
public class XmlConverter {

    private static final Logger log = LogManager.getLogger(XmlConverter.class);

    /**
     * Converts records from HashMap to XML
     * @param data HashMap with the records
     * @return XML
     * @throws CouldNotCreateXml if there were any troubles creating XML
     */
    public DOMSource convert(HashMap<RecordKey, Record> data)
            throws CouldNotCreateXml {
        try {
            log.info("Converting MAP to XML");
            Document document = getBuilder().newDocument();

            Element records = document.createElement("records");

            for (Map.Entry<RecordKey, Record> entry : data.entrySet()) {
                Element record = document.createElement("record");
                record.setAttribute(DBCaller.DEP_CODE_KEY, entry.getKey().getDepCode());
                record.setAttribute(DBCaller.DEP_JOB_KEY, entry.getKey().getDepJob());
                record.setAttribute(DBCaller.DESCRIPTION_STRING_KEY, entry.getValue().getDescriptionString());
                records.appendChild(record);
            }

            document.appendChild(records);

            DOMSource source = new DOMSource(document);
            return source;
        } catch (ParserConfigurationException e) {
            throw new CouldNotCreateXml("Could not create an xml from data", e);
        }
    }

    /**
     * Converts XML to HashMap
     * @param path to the XML file
     * @return HashMap with all the records
     * @throws CouldNotParseXml
     * @throws CouldNotSaveRecords
     */
    public HashMap<RecordKey, Record> convert(String path) throws CouldNotParseXml, CouldNotSaveRecords {
        HashMap<RecordKey, Record> records = new HashMap<>();
        try {
            log.info("Converting XML file to MAP to store in DB");
            Document document = getBuilder().parse(new File(path));
            document.getDocumentElement().normalize();
            NodeList list = document.getElementsByTagName("record");
            for (int i = 0; i < list.getLength(); i++) {
                Node record = list.item(i);
                if (record.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) record;
                    RecordKey recordKey = new RecordKey(
                                element.getAttribute(DBCaller.DEP_CODE_KEY),
                                element.getAttribute(DBCaller.DEP_JOB_KEY));
                    if (records.containsKey(recordKey)) {
                        System.out.println("ERROR:\nThere's more than one similar composition of dep_code and dep_job");
                        throw new CouldNotSaveRecords("Invalid XML");
                    }
                    records.put(recordKey,
                        new Record(element.getAttribute(DBCaller.DESCRIPTION_STRING_KEY)));
                }
            }

        } catch (SAXException | ParserConfigurationException | IOException e) {
            throw new CouldNotParseXml("Could not parse the xml file", e);
        }
        return records;
    }

    /**
     * Saves XML to a file
     * @param source XML source
     * @param path path where to save the XML
     * @throws CouldNotSaveXml
     */
    public void saveXml(DOMSource source, String path)
            throws CouldNotSaveXml {
        try {
            log.info("Saving XML");
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
            StreamResult result = new StreamResult(new File(path));
            transformer.transform(source, result);
        } catch (TransformerException e) {
            throw new CouldNotSaveXml("Could not save an xml to file", e);
        }
    }

    /**
     * to reduce boilerplate
     * @return builder
     * @throws ParserConfigurationException
     */
    private DocumentBuilder getBuilder() throws ParserConfigurationException {
        DocumentBuilderFactory factory
                    = DocumentBuilderFactory.newInstance();
        return factory.newDocumentBuilder();
    }

}
