package utils;

import exceptions.NoConnectionException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * A class to fetch the connections for all the methods with DB
 */
public class ConnectionFetcher {

    private static final Logger log = LogManager.getLogger(ConnectionFetcher.class);

    /**
     * Gets the connection by the url to the database
     * @return a connection
     * @throws NoConnectionException if connection could not be created by that url
     */
    protected static Connection getConnection() throws NoConnectionException {
        Connection connection = null;
        try {
            log.info("Getting a connection to the database");
            Properties properties = new Properties();
            InputStream inputStream = new FileInputStream("application.properties");
            properties.load(inputStream);
            connection = DriverManager.getConnection(
                    properties.getProperty("jdbc.url"));
            inputStream.close();
        } catch (SQLException | IOException e) {
            throw new NoConnectionException("Could not fetch the connection with database", e);
        }
        return connection;
    }

}
