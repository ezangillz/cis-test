package model;

public class Record {

    private String descriptionString;

    public Record(String descriptionString) {
        this.descriptionString = descriptionString;
    }

    public String getDescriptionString() {
        return descriptionString;
    }

    @Override
    public boolean equals(Object o) {
        Record record = (Record) o;
        return record.getDescriptionString().equals(getDescriptionString());
    }

    @Override
    public int hashCode() {
        return descriptionString.hashCode();
    }
}
