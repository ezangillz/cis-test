package model;

public class RecordKey {

    private String depCode;
    private String depJob;

    public RecordKey(String depCode, String depJob) {
        this.depCode = depCode;
        this.depJob = depJob;
    }

    public String getDepJob() {
        return depJob;
    }

    public String getDepCode() {
        return depCode;
    }

    @Override
    public boolean equals(Object o) {
        RecordKey key = (RecordKey) o;
        return key.getDepCode().equals(getDepCode()) &&
                key.getDepJob().equals(getDepJob());
    }

    @Override
    public int hashCode() {
        return new String(depJob + depCode).hashCode();
    }
}
