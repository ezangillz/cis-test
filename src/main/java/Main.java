import exceptions.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import utils.DBCaller;
import utils.XmlConverter;

public class Main {

    private static final String CONVERT_TO_XML_CMD = "to_xml";
    private static final String SYNC_DB_WITH_XML_CMD = "sync";
    private static final String HELP_CMD = "--help";

    private static final Logger log = LogManager.getLogger(Main.class);

    /**
     * Main method for "client-side"
     * @param args commands and parameters
     */
    public static void main(String[] args) {
        log.info("-------------START-------------");
        XmlConverter xmlConverter = new XmlConverter();

        try {
            String command = args[0];
            if (command.equals(CONVERT_TO_XML_CMD)) {
                String path = args[1];
                DBCaller dbCaller = new DBCaller();
                System.out.println("Saving to xml...");
                xmlConverter.saveXml(
                        xmlConverter.convert(dbCaller.getAllRecords()),
                        path);
                System.out.println("Done. Everything is in " + path);
            } else if (command.equals(SYNC_DB_WITH_XML_CMD)) {
                String path = args[1];
                DBCaller dbCaller = new DBCaller();
                System.out.println("Syncing database with the xml...");
                dbCaller.setAllRecords(xmlConverter.convert(path));
                System.out.println("Done.");
            } else if (command.equals(HELP_CMD)) {
                showHelp();
            } else {
                System.out.println("ERROR:\nUnknown command, use --help");
                log.error("Unknown command, use --help");
            }
        } catch (ArrayIndexOutOfBoundsException ex) {
            System.out.println("ERROR:\nNo parameters were passed, use --help");
        } catch (CouldNotFetchRecords e) {
            System.out.println("ERROR:\nCould not fetch the records from the database");
            log.error("Could not fetch the records from the database", e);
        } catch (CouldNotCreateXml e) {
            System.out.println("ERROR:\nCould not create the xml");
            log.error("Could not create the xml", e);
        } catch (CouldNotSaveXml e) {
            System.out.println("ERROR:\nCould not save the xml");
            log.error("Could not save the xml", e);
        } catch (CouldNotParseXml e) {
            System.out.println("ERROR:\nCould not parse the xml");
            log.error("Could not parse the xml", e);
        } catch (CouldNotSaveRecords e) {
            System.out.println("ERROR:\nCould not save the records");
            log.error("Could not save the records", e);
        }
        log.info("-------------END-------------");
    }

    public static void showHelp() {
        System.out.println("CIS TES:\n[Commands:]\t\t\t\t[Description:]" +
            "\n--help\t\t\t\t\tShows list of commands" +
            "\nto_xml [path]\t\t\tGets data from database and puts it to xml file in [path]" +
            "\nsync [path]\t\t\t\tGets data from xml file in [path] and synchronizes database with it");
    }

}
