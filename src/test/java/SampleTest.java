import org.junit.Test;

public class SampleTest {

    @Test
    public void toXmlTest() {
        Main.main(new String[] {"to_xml", "output.xml"});
    }

    @Test
    public void sync() {
        Main.main(new String[] {"sync", "output.xml"});
    }

    @Test
    public void invalidSync() {
        Main.main(new String[] {"sync"});
    }

    @Test
    public void invalidToXml() {
        Main.main(new String[] {"to_xml"});
    }

    @Test
    public void incorrectPathSync() {
        Main.main(new String[] {"sync", "asdaas"});
    }

    @Test
    public void incorrectPathToXml() {
        Main.main(new String[] {"to_xml", "asdsad"});
    }

    @Test
    public void incorrectFirstArgument() {
        Main.main(new String[] {"asdd"});
    }

    @Test
    public void noCommand() {
        Main.main(new String[] {});
    }

    @Test
    public void help() {
        Main.main(new String[] {"--help"});
    }

}
