import exceptions.*;
import model.Record;
import model.RecordKey;
import org.junit.Before;
import org.junit.Test;
import utils.DBCaller;
import utils.XmlConverter;

import javax.xml.transform.dom.DOMSource;
import java.util.HashMap;
import java.util.Map;

public class XmlTest {

    private static final String XML_PATH = "output.xml";

    private XmlConverter xmlConverter;
    private DBCaller dbCaller;
    private HashMap<RecordKey, Record> records;

    @Before
    public void init() throws CouldNotFetchRecords {
        xmlConverter = new XmlConverter();
        dbCaller = new DBCaller();
        records = dbCaller.getAllRecords();
    }

    @Test
    public void createXml() throws CouldNotCreateXml, CouldNotSaveXml {
        XmlConverter xmlConverter = new XmlConverter();
        xmlConverter.saveXml(xmlConverter.convert(records), XML_PATH);
    }

    @Test
    public void xmlToMap() throws CouldNotParseXml, CouldNotSaveRecords {
        XmlConverter xmlConverter = new XmlConverter();
        for (Map.Entry<RecordKey, Record> entry : xmlConverter.convert(XML_PATH).entrySet()) {
            System.out.println(entry.getKey().getDepCode() + " : " +
                    entry.getKey().getDepJob() + " : " +
                    entry.getValue().getDescriptionString());
        }
    }

    @Test
    public void sampleTest() throws CouldNotFetchRecords, CouldNotCreateXml, CouldNotSaveXml, CouldNotParseXml, CouldNotSaveRecords {
        records = dbCaller.getAllRecords();

        DOMSource source = xmlConverter.convert(records);
        xmlConverter.saveXml(source, XML_PATH);

        HashMap<RecordKey, Record> recordsFromXml = xmlConverter.convert(XML_PATH);

        assert recordsFromXml.equals(records);

        put("r", "k", "nothing");
        put("b", "b", "two");
        put("k", "w", "something");
        remove("c", "v");
        remove("b", "c");
        put("l", "m", "yeah");

        dbCaller.setAllRecords(records);

        records = dbCaller.getAllRecords();

        source = xmlConverter.convert(records);
        xmlConverter.saveXml(source, XML_PATH);

        recordsFromXml = xmlConverter.convert(XML_PATH);

        assert recordsFromXml.equals(records);
    }

    @Test
    public void showHelp() {
        Main.showHelp();
    }

    private void put(String depCode,
                     String depJob,
                     String description) {
        records.put(new RecordKey(depCode, depJob), new Record(description));
    }

    private void remove(String depCode,
                     String depJob) {
        records.remove(new RecordKey(depCode, depJob));
    }

}
