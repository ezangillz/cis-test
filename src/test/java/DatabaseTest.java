import exceptions.*;
import model.Record;
import model.RecordKey;
import org.junit.Before;
import org.junit.Test;
import utils.DBCaller;

import java.util.HashMap;

public class DatabaseTest {

    private DBCaller dbCaller;
    private HashMap<RecordKey, Record> records;

    @Before
    public void init() throws CouldNotFetchRecords {
        dbCaller = new DBCaller();
        records = dbCaller.getAllRecords();
    }


    @Test
    public void remove() throws CouldNotSaveRecords, CouldNotFetchRecords {
        records.put(new RecordKey("b", "c"), new Record("four"));
        records.put(new RecordKey("d", "d"), new Record("new record"));

        dbCaller.setAllRecords(records);

        records = dbCaller.getAllRecords();

        assert records.containsKey(new RecordKey("b", "c"));
        assert records.containsKey(new RecordKey("d", "d"));

        remove("d", "d");

        dbCaller.setAllRecords(records);

        records = dbCaller.getAllRecords();

        assert records.containsKey(new RecordKey("b", "c"));
        assert !records.containsKey(new RecordKey("d", "d"));
    }

    @Test
    public void insert() throws CouldNotFetchRecords, CouldNotSaveRecords {
        put("a", "b", "large");
        put("c", "v", "gore");
        put("m", "v", "c");

        dbCaller.setAllRecords(records);

        records = dbCaller.getAllRecords();

        assert records.containsKey(new RecordKey("a", "b"));
        assert records.containsKey(new RecordKey("c", "v"));
        assert records.containsKey(new RecordKey("m", "v"));
    }

    @Test
    public void update() throws CouldNotFetchRecords, CouldNotSaveRecords {
        put("a", "b", "large");

        dbCaller.setAllRecords(records);

        records = dbCaller.getAllRecords();

        assert records.get(new RecordKey("a", "b")).equals(new Record("large"));

        put("a", "b", "not large");

        dbCaller.setAllRecords(records);

        records = dbCaller.getAllRecords();

        assert records.get(new RecordKey("a", "b")).equals(new Record("not large"));
    }

    private void put(String depCode,
                     String depJob,
                     String description) {
        records.put(new RecordKey(depCode, depJob), new Record(description));
    }

    private void remove(String depCode,
                     String depJob) {
        records.remove(new RecordKey(depCode, depJob));
    }

}
