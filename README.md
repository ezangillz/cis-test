# Test for XML and DB

## Install

+ Run `mvn clean install`
+ It will create `test-1.0.0-jar-with-dependencies.jar`
+ Put `application.properties` next to this jar file, it should contain:
    + `jdbc.url` - url/path to the database

## REQUIREMENTS:

**!! DATABASE HAS TO HAVE records TABLE FOR APPLICATION TO WORK !!**

## Usage

+ java -jar test-1.0.0-jar-with-dependencies.jar to_xml [PATH_TO_XML] - takes records from the database and puts them into XML file
+ java -jar test-1.0.0-jar-with-dependencies.jar sync [PATH_TO_XML] - synchronizes the database with the XML file
